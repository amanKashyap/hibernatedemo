package org.project.dto;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;


@Entity(name="USER_DETAILS")
public class UserDetails 
{
	@Id
	@Column(name="USER_ID")
	private int userId;
	@Column(name="USER_NAME")
	private String userName;
	
	@AttributeOverrides({@AttributeOverride(name="street",column=@Column(name="HOME_STREET_NAME")),
						 @AttributeOverride(name="city",column=@Column(name="HOME_CITY_NAME")),
						 @AttributeOverride(name="state",column=@Column(name="HOME_STATE_NAME")),
						 @AttributeOverride(name="pincode",column=@Column(name="HOME_PINCODE"))
						})					
	private Address homeAddress;
	public Address getHomeAddress() {
		return homeAddress;
	}

	public void setHomeAddress(Address address) {
		this.homeAddress = address;
	}
	
	private Address officeAddress;
	public Address getOfficeAddress() {
		return officeAddress;
	}

	public void setOfficeAddress(Address address) {
		this.officeAddress = address;
	}

	public int getUserId() 
	{
		return userId;
	}
	
	public void setUserId(int userId) 
	{
		this.userId = userId;
	}
	
	public String getUserName() 
	{
		return userName;
	}
	public void setUserName(String userName) 
	{
		this.userName = userName;
	}

	
}
