package org.project.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.project.dto.Address;
import org.project.dto.UserDetails;

public class HibernateTest 
{

	/*public static void main(String[] args) 
	{
		UserDetails retrievedUser = new UserDetails();
		Load Configuration File - hibernate.cfg.xml
		  configure the details.
		  build session factory object 
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		//Create a New Session from Session Factory.
		Session session = sessionFactory.openSession();
	
		//Create a Transaction Object from session.
		session.beginTransaction();
		
		for(int i=0; i<10; i++)
		{
			UserDetails user = new UserDetails();
			user.setUserId(i);
			user.setUserName("First User");
			session.save(user);
		}
		
		session.getTransaction().commit();
		session.close();
		
		session = sessionFactory.openSession();
		session.beginTransaction();
		retrievedUser = session.get(UserDetails.class, 1);
		
		System.out.println(retrievedUser.getUserId());
		System.out.println(retrievedUser.getUserName());
		session.close();

	}*/
	
	public static void main(String[] args)
	{
		Address address = new Address();
		address.setStreet("Sample Street");
		address.setCity("Sample City");
		address.setState("Sample State");
		address.setPincode(123456);
		
		UserDetails user = new UserDetails();
		user.setUserId(1);
		user.setUserName("Sample");
		user.setHomeAddress(address);
		user.setOfficeAddress(address);
		
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		
		session.beginTransaction();
		
		session.save(user);
		//session.save(address);
		
		session.getTransaction().commit();
		session.close();
	}

}
